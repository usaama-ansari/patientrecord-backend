const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const patientSchema = new Schema({
    patientId: String,
    name: String,
    age: String,
    gender: String,
    altId: String,
    tests: [{
        specimen: String,
        physician: String,
        testName: String,
        serumValue: String,
        labNo: String,
        clinicalDiagnosis: String,
        investigations: [{ parameter: String, result: String }],
        dateOfCollection: String,
        dateOfTesting: String,
    }],
    dateCreated: { type: Date, default: Date.now() },
    lastUpdated: { type: Date, default: Date.now() }
});

const Patient = mongoose.model('patient', patientSchema)




module.exports = {
    Patient: Patient,

    addPatient: (receivedData) => {
        return new Promise((resolve, reject) => {
            var newPatient = new Patient(receivedData);
            newPatient.name = newPatient.name.toLowerCase();
            newPatient.save((err, savedPatient) => {
                if (err) {
                    reject();
                }
                else {
                    resolve(savedPatient);
                }
            });
        });
    },
    deletePatient: (_id) => {
        return new Promise((resolve, reject) => {
            Patient.deleteOne({ _id: _id }, (err, data) => {
                if (!err) {
                    resolve({success: true});                    
                }else{
                    resolve({success: false});   
                }
            })
        })
    },

    fetchPatients: (search_by, value) => {
        //query with name pr patient id 
        let query = {};
        if (value === null || value === '' || value === undefined) {
            query = {}
        } else {
            query = search_by === 'patientId' ? { patientId: { $regex: value, $options: 'i' } } : { name: { $regex: value, $options: 'i' } };
        }
        return new Promise((resolve, reject) => {
            try {
                Patient.find(query, {}, { sort: { _id: -1 } }, (err, patientList) => {
                    if (err) {
                        resolve({ success: false });
                    }
                    else {
                        resolve({ success: true, msg: patientList });
                    }
                });
            }
            catch (e) {
                resolve({ success: false });
            }

        });
    },

    editTest: (editedData) => {
        return new Promise((resolve, reject) => {
            let editedTest = editedData.editedTest;
            let editedTestIndex = editedData.editedTestIndex;
            let _id = editedData._id;
            Patient.findById(_id, (err, savedPatient) => {
                try {
                    if (err) throw err;
                    if (savedPatient) {
                        let updatedPatient = new Patient(savedPatient);
                        updatedPatient.tests[editedTestIndex] = editedTest;
                        updatedPatient.lastUpdated = Date.now();
                        updatedPatient.save((err, savedUpdatedPatient) => {
                            if (err) throw err;
                            resolve({ success: true, msg: { updatedPatient: savedUpdatedPatient } });
                        })
                    } else {
                        resolve({ success: false });
                    }

                }
                catch (e) {
                    resolve({ success: false });
                }
            });

        });
    },

    fetchForBackup(timeRange) {
        let query = {};
        if (timeRange === null || timeRange === '' || timeRange === undefined) {
            query = {};
        } else {
            let from = new Date(timeRange.from);
            let to = new Date(timeRange.to);
            query = { dateCreated: { $gte: from, $lte: to } }
        }

        return new Promise((resolve, reject) => {
            try {
                Patient.find(query, {}, { sort: { _id: -1 } }, (err, patientList) => {
                    if (err) {
                        resolve({ success: false });
                    }
                    else {
                        resolve({ success: true, msg: patientList });
                    }
                });
            }
            catch (e) {
                resolve({ success: false });
            }

        });




    }



}// END
