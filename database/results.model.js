const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const resultSchema = new Schema({
    resultname: {type:String, unique: true}
});



const  resultName = mongoose.model('resultname', resultSchema);
module.exports = {

    getSugg: function (typedResult) {
        return new Promise((resolve, reject) => {
            resultName.find({ resultname: { $regex: '^'+typedResult, $options: 'i' } }, { _id: 0,__v:0 }, (err, results) => {
                if (err) throw err;
                resolve(results)
            })
        })
    },
    addResult: function (receivedResultName) {
        let newResultName = new resultName({ resultname:receivedResultName });
        newResultName.save((err)=>{
            if(err) console.log('duplicate')
        });
       
    }

}