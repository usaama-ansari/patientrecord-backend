const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const parameterSchema = new Schema({
    parametername: {type:String, unique: true}
});



const  parameterName = mongoose.model('parametername', parameterSchema);
module.exports = {

    getSugg: function (typedParameter) {
        return new Promise((resolve, reject) => {
            parameterName.find({ parametername: { $regex: '^'+typedParameter, $options: 'i' } }, { _id: 0,__v:0 }, (err, parameters) => {
                if (err) throw err;
                resolve(parameters)
            })
        })
    },
    addParameter: function (receivedParameterName) {
        let newParameterName = new parameterName({ parametername:receivedParameterName });
        newParameterName.save((err)=>{
            if(err) console.log('duplicate')
        });
       
    }

}