const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const specimenSchema = new Schema({
    specimenname: {type:String, unique: true}
});



const specimen = mongoose.model('specimen', specimenSchema);
module.exports = {

    getSugg: function (typedName) {
       return new Promise((resolve, reject) => {
        specimen.find({ specimenname: { $regex: '^'+typedName, $options: 'i' } }, { _id: 0,__v:0 }, (err, names) => {
                if (err) throw err;
                resolve(names);
            })
       })
    },
    addName: function (receivedName) {
        let newName = new specimen({ specimenname:receivedName });
        newName.save((err)=>{
            if(err) console.log('');
        });
    }

}