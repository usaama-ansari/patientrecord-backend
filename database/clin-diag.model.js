const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const clinicaldiagnosisSchema = new Schema({
    cd: {type:String, unique: true}
});



const clinicalDiagnosis = mongoose.model('clindiag', clinicaldiagnosisSchema);
module.exports = {

    getSugg: function (typedCd) {
        return new Promise((resolve, reject) => {
            clinicalDiagnosis.find({ cd: { $regex: '^'+typedCd, $options: 'i' } }, { _id: 0,__v:0 }, (err, savedCds) => {
                if (err) throw err;
                resolve(savedCds);
            })
        })
    },
    addCd: function (receivedCd) {
        let newCd = new clinicalDiagnosis({ cd:receivedCd });
        newCd.save((err)=>{
            if(err) console.log('duplicate');
        });

    }

}