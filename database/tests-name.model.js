const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const testNameSchema = new Schema({
    testname: {type:String, unique: true}
});



const testName = mongoose.model('testname', testNameSchema);
module.exports = {

    getSugg: function (typedLabNo) {
        return new Promise((resolve, reject) => {
            testName.find({ testname: { $regex: '^'+typedLabNo, $options: 'i' } }, { _id: 0,__v:0 }, (err, labnos) => {
                if (err) throw err;
                resolve(labnos)
            })
        })
    },
    addTestName: function (receivedTestName) {
        let newTestName = new testName({ testname:receivedTestName });
        newTestName.save((err)=>{
            if(err) console.log('duplicate')
        });
       
    }

}