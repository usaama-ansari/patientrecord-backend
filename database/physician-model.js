const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const physicianSchema = new Schema({
    name: {type:String, unique: true}
});



const physician = mongoose.model('physician', physicianSchema);
module.exports = {

    getSugg: function (typedName) {
       return new Promise((resolve, reject) => {
        physician.find({ name: { $regex: '^'+typedName, $options: 'i' } }, { _id: 0,__v:0 }, (err, names) => {
                if (err) throw err;
                resolve(names);
            })
       })
    },
    addName: function (receivedName) {
        let newName = new physician({ name:receivedName });
        newName.save((err)=>{
            if(err) console.log('duplicate')
        });
    }

}