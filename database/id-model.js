const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const patientIdSchema = new Schema({
    _id: String,
    patient_id: Number
});



const patientId = mongoose.model('patientId', patientIdSchema);

module.exports = {

    getId: () => {
        return new Promise((resolve, reject) => {
            patientId.findOne({}, { _id: 0, patient_id: 1 }, (err, id) => {
                if (err) throw err;
                if (id) {
                    resolve({ success: true, msg: id })
                }
                else {
                    resolve({ success: false })
                }
            })
        })
    },

    incrId: () => {
        patientId.findOneAndUpdate({ _id: 'patientId' }, { $inc: { patient_id: 1 } }, { new: true, upsert: true }, (patient_id) => {
            // DO NOTHING
        });
    },

    setId: (id) => {
        id = id - 1;
        patientId.findOneAndUpdate({ _id: 'patientId' }, { $set: { patient_id: id } }, (err, data) => {
        });


    }

}