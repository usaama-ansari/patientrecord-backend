const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const labNoSchema = new Schema({
    labno: {type:String, unique: true}
});



const labNo = mongoose.model('labno', labNoSchema);
module.exports = {

    getSugg: function (typedLabNo) {
        return new Promise((resolve, reject) => {
            labNo.find({ labno: { $regex: '^'+typedLabNo, $options: 'i' } }, { _id: 0,__v:0 }, (err, labnos) => {
                if (err) throw err;
                resolve(labnos)
            })
        })
    },
    addLabNo: function (receivedLabNo) {
        let newLabNo = new labNo({ labno:receivedLabNo });
        newLabNo.save((err)=>{
            if(err) console.log('duplicate')
        });
       
    }

}