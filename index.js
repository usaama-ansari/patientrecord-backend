const electron = require('electron');
const mongoose = require('mongoose');
const { app, remote, BrowserWindow, ipcMain } = electron;
//##### ipcMain module is outsourced in ips.js
const IpcModule = require('./modules/ipc');



//============================================
//            DATABASE CLASS
//============================================
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/vrdl');
mongoose.connection.on('connected', function () {
    console.log('Mongoose connected');
});
mongoose.connection.on('error', function (err) {
    console.log('Database connection error');
});

//MAIN CLASS THAT INITIALIZES THE MAIN WINDOW
class App {
    constructor() {
        this.ipcModule = null;
        this.mainWindow = null;
        this.init_app();
    }
    init_app() {

        app.on('ready', () => {
            //==========================================
            //         MAIN WINDOW CREATION
            //==========================================
            this.mainWindow = new BrowserWindow({
                width: 1150,
                height: 650,
                minWidth: 1100,
                minHeight: 650,
                frame: false,
                webPreferences: { backgroundThrottling: false }
            });
            this.mainWindow.setMenu(null);
            this.mainWindow.loadURL(`file://${__dirname}/views/index.html`);
            // LOADING IPC MODULE CONTAINING DIFFERENT ipc EVENTS
            this.ipcModule = new IpcModule(this.mainWindow);
            this.mainWindow.on('closed', () => {
                mongoose.disconnect((err) => {
                    //do nothing
                });
                app.quit()
            })
        });
        //######### ON MAIN WINDOW CLOSE
        ipcMain.on('window:close', (event) => {
            app.quit();
        });
        //######### ON MAIN WINDOW MAXIMIZE
        ipcMain.on('window:max', (event) => {
            this.mainWindow.maximize();
        });
        //######### ON MAIN WINDOW MINIMIZE
        ipcMain.on('window:min', (event) => {
            this.mainWindow.minimize();
        });
    }
}

// CREATING INSTANCE OF APP
new App();
