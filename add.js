const mongoose = require('mongoose');
const Patient = require('./database/patient-model');
const fs = require('fs');



//============================================
//            DATABASE CLASS
//============================================
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/vrdl');
mongoose.connection.on('connected', function () {
    var data = fs.readFileSync('./dagta.json', 'utf8');
    data = JSON.parse(data);
    Patient.Patient.insertMany(data,(err,response)=>{
        if(err) console.log(err)
    })
});
mongoose.connection.on('error', function (err) {
    console.log('Database connection error');
});
