const fs = require('fs');
const csv = require('fast-csv');
const mongoose = require('mongoose');
const PatientModel = require('./database/patient-model');

const investigationConfig = {
    HbsAg: 'HbsAg',
    AntiHCV: 'AntiHCV',
    Dengue_IgM: 'Dengue IgM',
    Dengue_IgG: 'Dengue IgG',
    Dengue_NS_1_Antigen: 'Dengue NS-1 Antigen'
}
let restructured = [];


//============================================
//            DATABASE CLASS
//============================================
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/vrdl');
mongoose.connection.on('connected', function () {
    makeBackup();
});
mongoose.connection.on('error', function (err) {
    console.log('Database connection error');
});

function makeBackup() {
    PatientModel.fetchPatients(null, null).then((response) => {
        let patientsArray = response.msg;
        patientsArray.forEach((patient) => {
            patient.tests.forEach((test) => {
                let simplified = {
                    name: patient.name,
                    patientId: patient.patientId,
                    age: patient.age,
                    gender: patient.gender,
                    altId: patient.altId,
                    labNo: test.labNo,
                    clinicalDiagnosis: test.clinicalDiagnosis,
                    dateOfCollection: test.dateOfCollection,
                    dateOdTesting: test.dateOdTesting,
                    physician: test.physician,
                    specimen: test.specimen
                }
                test.investigations.forEach((investigation) => {
                    let keys = Object.keys(investigationConfig);
                    keys.forEach((key) => {
                        if (investigationConfig[key] === investigation.parameter) {
                            simplified[key] = investigation.result;
                        }
                    });
                });
                restructured.push(simplified);
            });
        });

        var csvStream = csv
            .createWriteStream({ headers: true })
            .transform(function (row) {
                return {
                    'PATIENT ID': row.patientId,
                    'NAME': row.name,
                    'AGE': row.age,
                    'GENDER': row.gender,
                    'OPD/CADS/IP No': row.altId,
                    'LAB NO': row.labNo,
                    'CLINICAL DIAGNOSIS': row.clinicalDiagnosis,
                    'DATE OF COLLECTION': row.dateOfCollection,
                    'DATE OF TESTING': row.dateOdTesting,
                    'PHYSICIAN': row.physician,
                    'SPECIMEN': row.specimen,
                    'HbsAg': row.HbsAg,
                    'AntiHCV': row.AntiHCV,
                    'Dengue IgM': row.Dengue_IgG,
                    'Dengue IgG': row.Dengue_IgG,
                    'Dengue NS-1 Antigen': row.Dengue_NS_1_Antigen
                };
            });

        writableStream = fs.createWriteStream("patients.csv");

        writableStream.on("finish", function () {
            console.log("DONE!");
        });


        csvStream.pipe(writableStream);
        restructured.forEach((patient) => {
            csvStream.write(patient);
        });
        csvStream.end();

    });

}


