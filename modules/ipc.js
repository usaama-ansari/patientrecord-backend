const electron = require('electron');
const { app, BrowserWindow, ipcMain } = electron;
const patientModel = require('../database/patient-model');
const patientIdModel = require('../database/id-model');
const SuggesstionsModule = require('./suggesstions.module');
const makeBackUp = require('./data-backup-module');
const path = require('path')
const moment = require('moment')
/*  
 *
 *  IPC API 
 * 
 */
class IpcModule {
    constructor(mainWindow) {
        this.printWindow = null;
        this.mainWindow = mainWindow;
        this.reportData = null;
        this.init_ipc();
    }
    //====================================================================
    //   INITIALIZE IPC MAIN EVENTS
    //====================================================================
    init_ipc() {
        // ########## ON NEW PATIENT CREATE
        ipcMain.on('patient:create', this.on_patient_create.bind(this));
        // ########## ON PATIENT DELETE
        ipcMain.on('patient:delete', this.on_patient_delete.bind(this));
        // ########## ON PATIENT LIST FETCH REQUEST
        ipcMain.on('patient:fetchList', this.on_fetch_list.bind(this));
        // ########## ON ID FETCH REQUEST WHITE NEW PATIENT RECORD
        ipcMain.on('patient:fetchId', this.fetch_id.bind(this));
        // ########## ON PATIENT EDIT
        ipcMain.on('patient:onTestEdit', this.on_patient_test_edit.bind(this));
        // ########## ON REPORT PRINT
        ipcMain.on('patient:onPrint', this.on_report_print.bind(this));
        // ########## ON MULTI REPORT PRINT
        ipcMain.on('patient:onMultiPrint', this.on_multi_report_print.bind(this));
        // ########## ON PRINT WINDOW OPENED
        ipcMain.on('printwindow:opened', this.on_print_window_opened.bind(this));
        // ########## ON REPORT PRINTED
        ipcMain.on('report:printed', this.on_report_printed.bind(this));
        // ########## ON BACKUP REQUEST
        ipcMain.on('patient:backUp', this.on_back_up.bind(this));
        // ########## ON SET ID
        ipcMain.on('patient:setId', this.on_set_id.bind(this));
        // ########## ON SUGGESSTION REQUEST RECEIVED
        ipcMain.on('suggesstions:request', this.on_suggesstions_request.bind(this));
    }

    // ID FETCHED BY FRONTEND TO SHOW IN FORM WHILE CREATING NEW PATIENT
    fetch_id() {
        patientIdModel.getId().then((response) => {
            this.mainWindow.webContents.send('patient:receiveId', JSON.stringify(response));
        })
    }
    // ID INCREAMENT BY ONE WHEN NEW RECORD IS ADDED SUCCESSFULLY
    incr_id() {
        patientIdModel.incrId();
    }

    on_patient_create(event, submittedForm) {
        //add to db
        patientModel.addPatient(submittedForm).then((savedPatient) => {
            this.incr_id();// increament id in database
            // add for suggesstions
            SuggesstionsModule.check_and_add(savedPatient);
            this.mainWindow.webContents.send('patient:created', { success: true });
        }).catch(() => {
            this.mainWindow.webContents.send('patient:created', { success: false });
        });
    }

    on_patient_delete(event, _id) {
        patientModel.deletePatient(_id).then((response) => {
            this.mainWindow.webContents.send('patient:deleted', response);
        });
    }

    // When list of existing patient is fetched frmo DB
    on_fetch_list(event, query) {
        patientModel.fetchPatients(query.search_by, query.value).then((response) => {
            this.mainWindow.webContents.send('patient:receivedList', JSON.stringify(response));
        });
    }

    // when pateient's test is edited
    on_patient_test_edit(event, editedData) {
        //SAVE EDITED TEST TO DB
        // edited data is : {editedTestIndex, editedTest}
        patientModel.editTest(editedData).then((response) => {
            this.mainWindow.webContents.send('patient:onTestEditComplete',
                JSON.stringify(response));
        });
    }

    on_report_print(event, reportData) {

        this.printWindow = new BrowserWindow({
            width: 1000,
            height: 750,
            webPreferences: { backgroundThrottling: false }
        });
        this.printWindow.setMenu(null);
        this.printWindow.loadURL(`file://${__dirname}/printreport.html`);
        this.printWindow.on('closed', () => {
            this.printWindow = null;
        });
        this.reportData = null;
        this.reportData = reportData;
    }
    on_multi_report_print(event, reportData) {
        this.printWindow = new BrowserWindow({
            width: 1000,
            height: 750,
            webPreferences: { backgroundThrottling: false }
        });
        this.printWindow.setMenu(null);
        this.printWindow.loadURL(`file://${__dirname}/multi-print.html`);
        this.printWindow.on('closed', () => {
            this.printWindow = null;
        });
        this.reportData = null;
        this.reportData = reportData;
    }
    on_print_window_opened() {
        this.printWindow.webContents.send('report:print', this.reportData)
    }

    on_set_id(event, id) {
        patientIdModel.setId(id);
    }

    on_report_printed() {
        //this.printWindow.close();
    }

    on_back_up(event, timeRange) {
        makeBackUp(timeRange).then(() => {
            this.mainWindow.webContents.send('patient:backUpDone');
        });
    }

    on_suggesstions_request(event, suggData) {
        if (suggData.value === '' || suggData.value === null || suggData.value === undefined) {
            return;
        } else {
            SuggesstionsModule.match_and_send(suggData.source, suggData.value).then((matchedSugg) => {
                let suggArray = matchedSugg.map((el) => {
                    let keyz = Object.keys(el.toJSON());
                    return el[keyz[0]];
                });
                let suggObject = { source: suggData.source, suggArray: suggArray }
                this.mainWindow.webContents.send('suggesstions:response', suggObject)
            });
        }
    }

}
module.exports = IpcModule
