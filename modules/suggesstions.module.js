const PhysicianModel = require('../database/physician-model');
const ClDiagModel = require('../database/clin-diag.model');
const TestNameModel = require('../database/tests-name.model');
const LabNoModel = require('../database/lab-no.model');
const ParametersModel = require('../database/parameters.model');
const ResultsModel = require('../database/results.model');
const SpecimenModel = require('../database/specimen-model');
const model_map = {
    'physician': PhysicianModel,
    'labno': LabNoModel,
    'cdiag': ClDiagModel,
    'testname': TestNameModel,
    'param': ParametersModel,
    'result': ResultsModel,
    'specimen': SpecimenModel
}

module.exports = {
    check_and_add: function (passedRecord) {
        let tests = passedRecord.tests;
        tests.forEach((test) => {
            let physician = test.physician;
            let clinicalDiagnosis = test.clinicalDiagnosis;
            let labNo = test.labNo;
            let testName = test.testName;
            let specimen = test.specimen;

            test.investigations.forEach((investigation) => {
                ParametersModel.addParameter(investigation.parameter);
                ResultsModel.addResult(investigation.result);
            });

            PhysicianModel.addName(physician);
            ClDiagModel.addCd(clinicalDiagnosis);
            LabNoModel.addLabNo(labNo);
            TestNameModel.addTestName(testName);
            SpecimenModel.addName(specimen);
        });
    },

    match_and_send: function (source, value) {
        return new Promise((resolve, reject) => {
            let target_model = model_map[source];
            target_model.getSugg(value).then((suggesstions) => {
                resolve(suggesstions);
            })
        })
    }
}