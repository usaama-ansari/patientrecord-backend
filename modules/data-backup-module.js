const fs = require('fs');
const csv = require('fast-csv');
const mongoose = require('mongoose');
const PatientModel = require('../database/patient-model');
const moment = require('moment');
const investigationConfig = {
    HbsAg: 'HbsAg',
    AntiHCV: 'AntiHCV',
    Dengue_IgM: 'Dengue IgM',
    Dengue_IgG: 'Dengue IgG',
    Dengue_NS_1_Antigen: 'Dengue NS-1 Antigen',
    HCV_IgG:'HCV(IgG)',
    HSV_1_IgM:'HSV-1(IgM)',
    HSV_2_IgM:'HSV-2(IgM)',
    Dengue_IgM:'Dengue IgM', 
    Dengue_IgG:'Dengue IgG', 
    Dengue_NS_1_Antigen:'Dengue NS-1 Antigen',
    Rotavirus_IgM:'Rotavirus IgM',
    Hepatitis_A_IgM:'Hepatitis A-IgM',
    Hepatitis_E_IgM:'Hepatitis E-IgM',
    Rotavirus_Antigen:'Rotavirus Antigen',
    JE_IgM:'JE-IgM',
    Chikunguniya_IgM:'Chikunguniya-IgM',
    Enterovirus_IgM:'Enterovirus-IgM',
    H3N2:'H3N2',
    Measles_IgM:'Measles-IgM',
    Panovirus_IgM:'Panovirus-IgM',
    Mumps_IgM:'Mumps-IgM',
    Rubella_IgM:'Rubella-IgM',
    Varicella_Zoaster_VZV_IgM:'Varicella Zoaster (VZV)-IgM',
    Epstein_Barrvirus_EBV_IgM:'Epstein-Barrvirus (EBV)-IgM',
}
let restructured = [];
makeBackup = function (timeRange) {
    return new Promise((resolve, reject) => {
        PatientModel.fetchForBackup(timeRange).then((response) => {
            let patientsArray = response.msg;
            patientsArray.forEach((patient) => {
                patient.tests.forEach((test) => {
                    let simplified = {
                        name: patient.name.toUpperCase(),
                        patientId: patient.patientId,
                        age: patient.age,
                        gender: patient.gender,
                        altId: patient.altId,
                        labNo: test.labNo.toUpperCase(),
                        clinicalDiagnosis: test.clinicalDiagnosis,
                        serumValue: test.serumValue = test.serumValue || 'N.A',
                        dateOfCollection: test.dateOfCollection,
                        dateOdTesting: test.dateOdTesting,
                        physician: test.physician.toUpperCase(),
                        specimen: test.specimen
                    }
                    var i = 1;
                    test.investigations.forEach((investigation) => {
                        simplified['parameter'+i++] = investigation.parameter+'/'+investigation.result;

                    //     let keys = Object.keys(investigationConfig);
                    //     var i = 1;
                    //     keys.forEach((key) => {
                    //         simplified['parameter'+i++] = key+'/'+investigation.result;
                    //         if (investigationConfig[key] === investigation.parameter) {
                    //             simplified['parameter'+i++] = key+'/'+investigation.result;
                    //         }
                    // console.log(simplified)

                    //     });
                    });
                    restructured.push(simplified);
                });
            });

            var csvStream = csv
                .createWriteStream({ headers: true })
                .transform(function (row) {
                    return {
                        'PATIENT ID': row.patientId,
                        'NAME': row.name,
                        'AGE': row.age,
                        'GENDER': row.gender,
                        'LAB NO': row.labNo,
                        'CLINICAL DIAGNOSIS': row.clinicalDiagnosis,
                        'DATE OF COLLECTION': row.dateOfCollection,
                        'DATE OF TESTING': row.dateOdTesting,
                        'PHYSICIAN': row.physician,
                        'SPECIMEN': row.specimen,
                        'INVESTIGATION_1': row.parameter1,
                        'INVESTIGATION_2': row.parameter2,
                        'INVESTIGATION_3': row.parameter3,
                        'INVESTIGATION_4': row.parameter4,
                        'INVESTIGATION_5': row.parameter5,
                        'INVESTIGATION_6': row.parameter6,
                        'INVESTIGATION_7': row.parameter7,
                        'INVESTIGATION_8': row.parameter8
                        // 'HbsAg': row.HbsAg,
                        // 'AntiHCV': row.AntiHCV,
                        // 'Dengue IgM': row.Dengue_IgG,
                        // 'Dengue IgG': row.Dengue_IgG,
                        // 'Dengue NS-1 Antigen': row.Dengue_NS_1_Antigen
                    };
                });
            // check for BACKUP DIRECTORY EXIST
            let dir_path = process.env.HOMEDRIVE + process.env.HOMEPATH + '\\Desktop\\vrdl_backup';
            fs.exists(dir_path, (exists) => {
                if (!exists) {
                    fs.mkdir(dir_path, (err) => {
                        if (!err) {

                            start_backup(dir_path);
                        }
                        else {
                            console.log(err)
                        }
                    });
                }
                else {
                    start_backup(dir_path);
                }
            })

            function start_backup(path) {
                let today = moment().format('DD-MM-YYYY');
                writableStream = fs.createWriteStream(path + "\\vrdl_" + today + '_' + Date.now() + ".csv");
                writableStream.on("finish", function () {
                    resolve();
                });
                csvStream.pipe(writableStream);
                restructured.forEach((patient) => {
                    csvStream.write(patient);
                });
                csvStream.end();
            }

        });
    });

}


module.exports = makeBackup;

